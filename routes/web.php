<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/catalog', function () {
    return view('catalog');
});

Route::get('/catalog/medicine', function () {
    return view('list', ['name' => 'medicine']);
});

Route::get('/catalog/med_equip', function () {
    return view('list', ['name' => 'med_equip']);
});

Route::get('/medicines/tremenza', function () {
    return view('detail', ['id' => 'med1']);
});

Route::get('/medicines/panadol', function () {
    return view('detail', ['id' => 'med2']);
});

Route::get('/medicines/promag', function () {
    return view('detail', ['id' => 'med3']);
});

Route::get('/equipments/handsanitizer', function () {
    return view('detail', ['id' => 'equ1']);
});

Route::get('/equipments/suntik', function () {
    return view('detail', ['id' => 'equ2']);
});



