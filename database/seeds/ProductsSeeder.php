<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('products')->insert([
        'Generic Name' => 'fentanil',
        'Form' => 'inj 0,05 mg/mL (i.m./i.v.)',
        'Restriction and Formula' => '5 amp/kasus.',
        'Description' => 'inj: Hanya untuk nyeri berat dan harus diberikan oleh tim medis yang dapat melakukan resusitasi.',
        'Faskes TK1' => '0',
        'Faskes TK2' => '1',
        'Faskes TK3' => '1',
        'Category' => '1'
    ]);


    DB::table('products')->insert([
        'Generic Name' => 'fentanil',
        'Form' => 'patch 12,5 mcg/jam',
        'Restriction and Formula' => '10 patch/bulan.',
        'Description' => 'patch: Untuk nyeri kronik pada pasien kanker yang tidak terkendali. Tidak untuk nyeri akut.',
        'Faskes TK1' => '0',
        'Faskes TK2' => '1',
        'Faskes TK3' => '1',
        'Category' => '1'
    ]);


    DB::table('products')->insert([
        'Generic Name' => 'fentanil',
        'Form' => 'patch 25 mcg/jam',
        'Restriction and Formula' => '10 patch/bulan.',
        'Description' => 'patch: Untuk nyeri kronik pada pasien kanker yang tidak terkendali. Tidak untuk nyeri akut.',
        'Faskes TK1' => '0',
        'Faskes TK2' => '1',
        'Faskes TK3' => '1',
        'Category' => '1'
    ]);


    DB::table('products')->insert([
        'Generic Name' => 'asam mefenamat',
        'Form' => 'kaps 250 mg',
        'Restriction and Formula' => '30 kaps/bulan.',
        'Description' => '',
        'Faskes TK1' => '1',
        'Faskes TK2' => '1',
        'Faskes TK3' => '1',
        'Category' => '2'
    ]);


    DB::table('products')->insert([
        'Generic Name' => 'asam mefenamat',
        'Form' => 'tab 500 mg',
        'Restriction and Formula' => '30 tab/bulan.',
        'Description' => '',
        'Faskes TK1' => '1',
        'Faskes TK2' => '1',
        'Faskes TK3' => '1',
        'Category' => '2'
    ]);


    DB::table('products')->insert([
        'Generic Name' => 'ibuprofen*',
        'Form' => 'tab 200 mg',
        'Restriction and Formula' => '30 tab/bulan.',
        'Description' => '',
        'Faskes TK1' => '1',
        'Faskes TK2' => '1',
        'Faskes TK3' => '1',
        'Category' => '2'
    ]);


    DB::table('products')->insert([
        'Generic Name' => 'alopurinol',
        'Form' => 'tab 100 mg*',
        'Restriction and Formula' => '30 tab/bulan.',
        'Description' => 'Tidak diberikan pada saat nyeri akut.',
        'Faskes TK1' => '1',
        'Faskes TK2' => '1',
        'Faskes TK3' => '1',
        'Category' => '3'
    ]);


    DB::table('products')->insert([
        'Generic Name' => 'kolkisin',
        'Form' => 'tab 500 mcg',
        'Restriction and Formula' => '30 tab/bulan.',
        'Description' => '',
        'Faskes TK1' => '1',
        'Faskes TK2' => '1',
        'Faskes TK3' => '1',
        'Category' => '3'
    ]);


    DB::table('products')->insert([
        'Generic Name' => 'probenesid',
        'Form' => 'tab 500 mg',
        'Restriction and Formula' => '30 tab/bulan.',
        'Description' => '',
        'Faskes TK1' => '1',
        'Faskes TK2' => '1',
        'Faskes TK3' => '1',
        'Category' => '3'
    ]);


    DB::table('products')->insert([
        'Generic Name' => 'amitriptilin',
        'Form' => 'tab 25 mg',
        'Restriction and Formula' => '30 tab/bulan.',
        'Description' => '',
        'Faskes TK1' => '0',
        'Faskes TK2' => '1',
        'Faskes TK3' => '1',
        'Category' => '4'
    ]);


    DB::table('products')->insert([
        'Generic Name' => 'gabapentin',
        'Form' => 'kaps 100 mg',
        'Restriction and Formula' => '60 kaps/bulan.',
        'Description' => 'Hanya untuk neuralgia pascaherpes atau nyeri neuropati diabetikum.',
        'Faskes TK1' => '0',
        'Faskes TK2' => '1',
        'Faskes TK3' => '1',
        'Category' => '4'
    ]);


    DB::table('products')->insert([
        'Generic Name' => 'karbamazepin',
        'Form' => 'tab 100 mg',
        'Restriction and Formula' => '60 tab/bulan.',
        'Description' => '',
        'Faskes TK1' => '1',
        'Faskes TK2' => '1',
        'Faskes TK3' => '1',
        'Category' => '4'
    ]);


    DB::table('products')->insert([
        'Generic Name' => 'bupivakain',
        'Form' => 'inj 0,5%',
        'Restriction and Formula' => '',
        'Description' => '',
        'Faskes TK1' => '0',
        'Faskes TK2' => '1',
        'Faskes TK3' => '1',
        'Category' => '5'
    ]);


    DB::table('products')->insert([
        'Generic Name' => 'bupivakain heavy',
        'Form' => 'inj 0,5% + glukosa 8%',
        'Restriction and Formula' => '',
        'Description' => 'Khusus untuk analgesia spinal.',
        'Faskes TK1' => '0',
        'Faskes TK2' => '1',
        'Faskes TK3' => '1',
        'Category' => '5'
    ]);


    DB::table('products')->insert([
        'Generic Name' => 'etil klorida',
        'Form' => 'spray 100 mL',
        'Restriction and Formula' => '',
        'Description' => '',
        'Faskes TK1' => '1',
        'Faskes TK2' => '1',
        'Faskes TK3' => '1',
        'Category' => '5'
    ]);


    DB::table('products')->insert([
        'Generic Name' => 'deksmedetomidin',
        'Form' => 'inj 100 mcg/mL',
        'Restriction and Formula' => '',
        'Description' => 'Untuk sedasi pada pasien di ICU, kraniotomi, bedah jantung dan operasi yang memerlukan waktu pembedahan yang lama.',
        'Faskes TK1' => '0',
        'Faskes TK2' => '1',
        'Faskes TK3' => '1',
        'Category' => '6'
    ]);


    DB::table('products')->insert([
        'Generic Name' => 'desfluran',
        'Form' => 'ih',
        'Restriction and Formula' => '',
        'Description' => '',
        'Faskes TK1' => '0',
        'Faskes TK2' => '1',
        'Faskes TK3' => '1',
        'Category' => '6'
    ]);


    DB::table('products')->insert([
        'Generic Name' => 'halotan',
        'Form' => 'ih',
        'Restriction and Formula' => '',
        'Description' => 'Tidak boleh digunakan berulang. Tidak untuk pasien dengan gangguan fungsi hati.',
        'Faskes TK1' => '0',
        'Faskes TK2' => '1',
        'Faskes TK3' => '1',
        'Category' => '6'
    ]);


    DB::table('products')->insert([
        'Generic Name' => 'atropin',
        'Form' => 'inj 0,25 mg/mL (i.v./s.k.)',
        'Restriction and Formula' => '',
        'Description' => '',
        'Faskes TK1' => '1',
        'Faskes TK2' => '1',
        'Faskes TK3' => '1',
        'Category' => '7'
    ]);


    DB::table('products')->insert([
        'Generic Name' => 'diazepam',
        'Form' => 'inj 5 mg/mL',
        'Restriction and Formula' => '',
        'Description' => '',
        'Faskes TK1' => '1',
        'Faskes TK2' => '1',
        'Faskes TK3' => '1',
        'Category' => '7'
    ]);


    DB::table('products')->insert([
        'Generic Name' => 'midazolam',
        'Form' => 'inj 1 mg/mL (i.v.)',
        'Restriction and Formula' => 'Dosis rumatan: 1 mg/jam (24 mg/hari). Dosis premedikasi: 8 vial/kasus.',
        'Description' => 'Dapat digunakan untuk premedikasi sebelum induksi anestesi dan rumatan selama anestesi umum.',
        'Faskes TK1' => '0',
        'Faskes TK2' => '1',
        'Faskes TK3' => '1',
        'Category' => '7'
    ]);
    

    DB::table('products')->insert([
        'Generic Name' => 'deksametason',
        'Form' => 'inj 5 mg/mL',
        'Restriction and Formula' => '20 mg/hari.',
        'Description' => '',
        'Faskes TK1' => '1',
        'Faskes TK2' => '1',
        'Faskes TK3' => '1',
        'Category' => '8'
    ]);

    
    DB::table('products')->insert([
        'Generic Name' => 'difenhidramin',
        'Form' => 'inj 10 mg/mL (i.v./i.m.)',
        'Restriction and Formula' => '30 mg/hari.',
        'Description' => '',
        'Faskes TK1' => '1',
        'Faskes TK2' => '1',
        'Faskes TK3' => '1',
        'Category' => '8'
    ]);

    
    DB::table('products')->insert([
        'Generic Name' => 'epinefrin (adrenalin)',
        'Form' => 'inj 1 mg/mL',
        'Restriction and Formula' => '',
        'Description' => '',
        'Faskes TK1' => '1',
        'Faskes TK2' => '1',
        'Faskes TK3' => '1',
        'Category' => '8'
    ]);


    DB::table('products')->insert([
        'Generic Name' => 'atropin',
        'Form' => 'tab 0,5 mg',
        'Restriction and Formula' => '',
        'Description' => '',
        'Faskes TK1' => '1',
        'Faskes TK2' => '1',
        'Faskes TK3' => '1',
        'Category' => '9'
    ]);


    DB::table('products')->insert([
        'Generic Name' => 'efedrin',
        'Form' => 'inj 50 mg/mL',
        'Restriction and Formula' => '',
        'Description' => '',
        'Faskes TK1' => '0',
        'Faskes TK2' => '1',
        'Faskes TK3' => '1',
        'Category' => '9'
    ]);


    DB::table('products')->insert([
        'Generic Name' => 'nalokson',
        'Form' => 'inj 0,4 mg/mL',
        'Restriction and Formula' => '',
        'Description' => 'Hanya untuk mengatasi depresi pernapasan akibat morfin atau opioid.',
        'Faskes TK1' => '0',
        'Faskes TK2' => '1',
        'Faskes TK3' => '1',
        'Category' => '9'
    ]);


    DB::table('products')->insert([
        'Generic Name' => 'diazepam',
        'Form' => 'inj 5 mg/mL',
        'Restriction and Formula' => '10 amp/kasus, kecuali untuk kasus di ICU.',
        'Description' => 'Tidak untuk i.m.',
        'Faskes TK1' => '1',
        'Faskes TK2' => '1',
        'Faskes TK3' => '1',
        'Category' => '11'
    ]);


    DB::table('products')->insert([
        'Generic Name' => 'fenitoin',
        'Form' => 'kaps 30 mg*',
        'Restriction and Formula' => '90 kaps/bulan.',
        'Description' => 'Dapat digunakan untuk status konvulsivus.',
        'Faskes TK1' => '1',
        'Faskes TK2' => '1',
        'Faskes TK3' => '1',
        'Category' => '11'
    ]);


    DB::table('products')->insert([
        'Generic Name' => 'fenobarbital',
        'Form' => 'tab 30 mg*',
        'Restriction and Formula' => '120 tab/bulan.',
        'Description' => '',
        'Faskes TK1' => '1',
        'Faskes TK2' => '1',
        'Faskes TK3' => '1',
        'Category' => '11'
    ]);
    }
}
