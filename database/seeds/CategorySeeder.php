<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            'Name' => 'ANALGESIK NARKOTIK',
            'Description' => ''
        ]);

        DB::table('categories')->insert([
            'Name' => 'ANALGESIK NON NARKOTIK',
            'Description' => ''
        ]);

        DB::table('categories')->insert([
            'Name' => 'ANTIPIRAI',
            'Description' => ''
        ]);

        DB::table('categories')->insert([
            'Name' => 'NYERI NEUROPATIK',
            'Description' => ''
        ]);

        DB::table('categories')->insert([
            'Name' => 'ANESTETIK LOKAL',
            'Description' => ''
        ]);

        DB::table('categories')->insert([
            'Name' => 'ANESTETIK UMUM dan OKSIGEN',
            'Description' => ''
        ]);

        DB::table('categories')->insert([
            'Name' => 'OBAT untuk PROSEDUR PRE OPERATIF',
            'Description' => ''
        ]);

        DB::table('categories')->insert([
            'Name' => 'ANTIALERGI dan OBAT untuk ANAFILAKSIS',
            'Description' => ''
        ]);

        DB::table('categories')->insert([
            'Name' => 'KHUSUS',
            'Description' => ''
        ]);

        DB::table('categories')->insert([
            'Name' => 'UMUM',
            'Description' => ''
        ]);

        DB::table('categories')->insert([
            'Name' => 'ANTIEPILEPSI - ANTIKONVULSI',
            'Description' => ''
        ]);
    }
}
