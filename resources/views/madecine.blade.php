<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <title>Catalog</title>

    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 13px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }
    </style>
  </head>
  <body>
    <div class="container">
      @if ($name == "medicine")
        <div class="content">
          <div class="title m-b-md">
              Medicine
          </div>
      </div>
      <div class="row row-cols-1 row-cols-md-2 g-4">
            <img src="https://d2qjkwm11akmwu.cloudfront.net/products/849685_22-9-2021_10-21-15.png">
            <div>
              <h5 class="card-title">Tremenza</h5>
              <a href="/medicines/tremenza" class="btn btn-primary">Detail</a>
            </div>


            <img src="https://images.tokopedia.net/img/cache/700/VqbcmM/2021/9/22/ae80db2d-7720-4a30-8029-786fea4e495a.jpg" >
            <div class="card-body">
              <h5 class="card-title">Promag</h5>
              <a href="/medicines/promag" class="btn btn-primary">Detail</a>
            </div>


            <img src="https://images.tokopedia.net/img/cache/700/product-1/2020/7/1/5372526/5372526_424be0d8-ece4-43db-83f5-62b0fd2036dd_1080_1080.jpg">
            <div>
              <h5 class="card-title">Panadol</h5>
              <a href="/medicines/panadol" class="btn btn-primary">Detail</a>
            </div>
              
      

      @elseif ($name == "med_equip")
        <div class="content">
          <div class="title m-b-md">
              Medical Equipment
          </div>
      </div>
        <div class="row row-cols-1 row-cols-md-2 g-4">
            <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR3YOdovQkjYHD68Fxuz1_5IYx61trRau6gqw&usqp=CAU">
              <h5 class="card-title">Masker</h5>
              <a href="/equipments/masker" class="btn btn-primary">Detail</a>
            </div>
            <div>
            <img src="https://cf.shopee.co.id/file/b3e99999570b771c176b573c6bea365c">
              <h5 class="card-title">HandSanitizer</h5>
              <a href="/equipments/handsanitizer" class="btn btn-primary">Detail</a>
            </div>
            
      @endif
    </div>
  </div>
  </body>
</html>