<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <title>Detail</title>
  </head>
  <body>

    @if($id == "med1")
    <?php
        $url = "https://lifepack.id/wp-content/uploads/2021/04/TREETH602-lifepack-pict1.jpg";
        $nama = "Tremenza";
        $keterangan = "Obat tremenza digunakan untuk meredakan gejala-gejala flu karena alergi pada saluran pernafasan."
    ?>
    @elseif($id == "med2")
    <?php
        $url = "https://images.tokopedia.net/img/cache/700/product-1/2020/7/1/5372526/5372526_424be0d8-ece4-43db-83f5-62b0fd2036dd_1080_1080.jpg";
        $nama = "Panadol";
        $keterangan = "Obat panadol digunakan untuk meredakan nyeri seperti sakit kepala,sakit gigi, dan nyeri otot. "
    ?>
    @elseif($id == "med3")
    <?php
        $url = "https://images.tokopedia.net/img/cache/700/VqbcmM/2021/9/22/ae80db2d-7720-4a30-8029-786fea4e495a.jpg";
        $nama = "Promag";
        $keterangan = "Obat promag digunakan untuk mengatasi sakit maag, penyakit asam lambung naik, dan perut kembung"
    ?>
    @elseif($id == "equ1")
    <?php
        $url = "https://cf.shopee.co.id/file/b3e99999570b771c176b573c6bea365c";
        $nama = "Hand Sanitizer";
        $keterangan = "Membunuh kuman yang hinggap di tangan"
    ?>
    @elseif($id == "equ2")
    <?php
        $url = "https://d1bpj0tv6vfxyp.cloudfront.net/articles/649748_2-3-2021_13-33-16.jpeg";
        $nama = "Suntik";
        $keterangan = "Alat memasukan cairan kedalam pembuluh darah"
    ?>
    @endif

    <div class="container">
        <div class="card mb-3">
            <img src="<?php echo $url ?>" class="card-img-top">
            <div class="card-body">
              <h5 class="card-title">{{ $nama }}</h5>
              <p class="card-text">{{ $keterangan }}</p>
            </div>
          </div>
      </body>
    </div>
</html>